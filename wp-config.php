<?php
/** 
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information by
 * visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'oakurbanlandscaping');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'password');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link http://api.wordpress.org/secret-key/1.1/ WordPress.org secret-key service}
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Qt2ftywcPf8OqGZigdgFAJ@jee9t@XMR%^fjadRuHaXpLEtgZwQ2azlxFxY@ZOGT');
define('SECURE_AUTH_KEY',  'c)@#EeztQtym)4P1S5Og$g%9kowYR5vyO#MBlLSn*Qmdm2BUM(YSxS8txKLc25qL');
define('LOGGED_IN_KEY',    '3WZTgNHDSy$#4NvI3@nLEp$YzJ9#wFTh$4!W)T$JC#sTfQL5KJgw3YWjltyAl76D');
define('NONCE_KEY',        '*pdw6PiogjTC43jYTVNO36rqmR)1L9W1jP)WCAPL%3H#XKQ$q%*TbcexMg3oSRM*');
define('AUTH_SALT',        'YpyvkY!QPo2zlc^y(R7GlGFCg73GXFr6&FShwt5d7eu#BeBwp2Xwl86OOx5jW2m9');
define('SECURE_AUTH_SALT', 'mt@)p$B&5Xe1TN)R7q!nLt$OJRKjB)g6E(%tV*eK1$U3rjsJL2E1ykct3Be(zi6I');
define('LOGGED_IN_SALT',   'VEpKhJeUXzGUMnGTvoQf$%g2sPHi7z8f7hm6FMV@fC!)5sYzu($ex3o&yfaeZqL6');
define('NONCE_SALT',       'yCvzoGJrMollDTw1w&iwTZY8EKBRbCSP3v%8%SUg95@wX6NwF32p2K8g#eX(Etf(');
/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress.  A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de.mo to wp-content/languages and set WPLANG to 'de' to enable German
 * language support.
 */
define ('WPLANG', 'en_US');

define ('FS_METHOD', 'direct');

define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');


//--- disable auto upgrade
define( 'AUTOMATIC_UPDATER_DISABLED', true );

?>
