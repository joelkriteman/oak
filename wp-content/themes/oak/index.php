<?php
	$uri_test = substr(strchr($_SERVER['REQUEST_URI'], 'portfolio'), 0, 9);
	if ($uri_test == 'portfolio') {
		require 'page-portfolio.php';
	}
	else {
		get_header(); ?>

	<div id="content" class="widecolumn">

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
										
				<div class="entry">
					<?php the_content(); ?>
				</div>
				
			</div>

		<?php endwhile; ?>
	<?php endif; ?>

	</div>

<?php
	}
	get_footer(); ?>