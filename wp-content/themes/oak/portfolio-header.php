<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
		<head profile="http://gmpg.org/xfn/11">
		<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
		<title><?php if (is_home () ) { bloginfo('name'); } elseif (is_category() || is_tag()) { single_cat_title(); echo ' &bull; ' ; bloginfo('name'); } elseif (is_single() || is_page()) { single_post_title(); } else { wp_title('',true); } ?></title>
		<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
		<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
		<link rel="shortcut icon" href="<?php echo get_bloginfo('template_url') ?>/images/favicon.ico" />
		<?php wp_head(); ?>
		</head>
		<body <?php body_class(); ?>>

		<div id="site">
		<div id="wrap">
			<div id="mainmenu">
				<div id="portfolionumbers">
					<?php 
						$parentarray = get_post_ancestors($post->ID);
						$parentpage = $parentarray[0];
						$children = get_pages('child_of='.$parentpage);
						$index = 1;
						$linebreak = 5;
						$uri_test = substr(strchr($_SERVER['REQUEST_URI'], 'portfolio'), 10, -1);
						while ($index <= sizeof($children)) {
							if ($uri_test == get_the_title($children[$index-1]->ID)) {
								echo '<a href="' . get_bloginfo('wpurl') . '/portfolio/' . get_the_title($children[$index-1]->ID) . '" class="active">' . $index . '</a>' . '&nbsp;&nbsp;&nbsp;';
								if ($index % $linebreak == '0') {
									echo '<br /><br />';
								}
							}
							else {
								echo '<a href="' . get_bloginfo('wpurl') . '/portfolio/' . get_the_title($children[$index-1]->ID) . '">' . $index . '</a>' . '&nbsp;&nbsp;&nbsp;';
								if ($index % $linebreak == '0') {
									echo '<br /><br />';
								}
							}
							$index++;
						}
					?>
				</div>
				<div id="portfolio-thumbnails">
<!--					<?php
					$args = array( 'post_type' => 'attachment', 'numberposts' => -1, 'post_status' => null, 'post_parent' => $post->ID, 'size' => 'thumbnail' ); 
					$attachments = get_posts($args);
					sort($attachments);
					if ($attachments) {
						foreach ( $attachments as $attachment ) {
							the_attachment_link( $attachment->ID , false );
						}
					}
					?>
-->
				</div>
			</div>
			<div id="rightmenu">
				<?php wp_nav_menu( array( 'theme_location' => 'rightmenu')); ?>
			</div>
			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Submenu') ) : ?><?php endif; ?>
		<div id="blog">