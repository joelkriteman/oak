<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<title><?php if (is_home () ) { bloginfo('name'); } elseif (is_category() || is_tag()) { single_cat_title(); echo ' &bull; ' ; bloginfo('name'); } elseif (is_single() || is_page()) { single_post_title(); } else { wp_title('',true); } ?></title>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<link rel="shortcut icon" href="<?php echo get_bloginfo('template_url') ?>/images/favicon.ico" />
<?php wp_head(); ?>


</head>
<body <?php body_class(); ?>>

<div id="site">
<div id="wrap">
	<div id="mainmenu">
		<div class="menuitem">
			<a href="<?php echo get_bloginfo('wpurl') ?>/2011/about"><img src="<?php echo get_bloginfo('template_url') ?>/images/about.jpeg" alt="about" /></a> <br />
			<a href="<?php echo get_bloginfo('wpurl') ?>/2011/about">about</a>
		</div>
		<div class="menuitem">
			<a href="<?php echo get_bloginfo('wpurl') ?>/2011/design"><img src="<?php echo get_bloginfo('template_url') ?>/images/design.jpeg" alt="about" /></a> <br />
			<a href="<?php echo get_bloginfo('wpurl') ?>/2011/design">design</a>
		</div>
		<div class="menuitem">
			<a href="<?php echo get_bloginfo('wpurl') ?>/2011/lighting"><img src="<?php echo get_bloginfo('template_url') ?>/images/lighting.jpeg" alt="about" /></a> <br />
			<a href="<?php echo get_bloginfo('wpurl') ?>/2011/lighting">lighting</a>
		</div>
		<div class="menuitem">
			<a href="<?php echo get_bloginfo('wpurl') ?>/2011/materials"><img src="<?php echo get_bloginfo('template_url') ?>/images/materials.jpeg" alt="about" /></a> <br />
			<a href="<?php echo get_bloginfo('wpurl') ?>/2011/materials">materials</a>
		</div>
		<div class="menuitem">
			<a href="<?php echo get_bloginfo('wpurl') ?>/2011/planting"><img src="<?php echo get_bloginfo('template_url') ?>/images/planting.jpeg" alt="about" /></a> <br />
			<a href="<?php echo get_bloginfo('wpurl') ?>/2011/planting">planting</a>
		</div>
	
	</div>
	<div id="rightmenu">
		<?php wp_nav_menu( array( 'theme_location' => 'rightmenu')); ?>
	</div>
	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Submenu') ) : ?><?php endif; ?>
<div id="blog">