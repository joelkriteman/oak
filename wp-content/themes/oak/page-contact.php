<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<head>
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
	<style type="text/css">
		#contact-background {
			background-image: url(<?php echo get_bloginfo('template_url') ?>/images/contact_background.gif);
			background-repeat: no-repeat;
			background-position: 50% 0%;
		}
		
		body {
			background-color: #868686;
			
		}
	</style>
	<title>Contact</title>
</head>	
<body>
	<div class="contact-page">
		<div id="contact-background">
			<div id="contact-menu">
				<?php wp_nav_menu( array( 'theme_location' => 'rightmenu')); ?>
			</div>
			<div id="contact-content">
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>									
						<div class="entry">
							<?php the_content(); ?>
						</div>
					</div>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</body>

