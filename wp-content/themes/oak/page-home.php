<?php get_header(); ?>

	<div id="content" class="widecolumn">

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
										
				<div class="entry">
					<?php the_content(); ?>
					</div>
				</div>
				
			</div>

		<?php endwhile; ?>
	<?php endif; ?>

	</div>

<?php get_footer(); ?>