<?php
	$children = get_pages('child_of='.get_post_ancestors($post)->ID);
	// porfolio pages existing
	if( count( $children ) != 0 ) {
		require ('portfolio-header.php');
		?>
		
		<div id="content" class="widecolumn">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
											
					<div class="entry">
						<?php the_content(); ?>
					</div>
					
				</div>

			<?php endwhile; ?>
		<?php endif; ?>

		</div>
		<?php
	}
	
	// no portfolio pages found
	else { ?>
		<?php get_header(); ?>

		<div id="content" class="widecolumn">

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
											
					<div class="entry">
					</div>
					
				</div>

			<?php endwhile; ?>
		<?php endif; ?>

		</div>
	<?php 
	}
	?>



<?php get_footer(); ?>