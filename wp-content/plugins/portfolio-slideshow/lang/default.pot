msgid ""
msgstr ""
"Project-Id-Version: Portfolio Slideshow\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2011-04-13 10:14-0500\n"
"PO-Revision-Date: 2011-04-13 10:15-0500\n"
"Last-Translator: \n"
"Language-Team: Raygun <info@madbyraygun.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-KeywordsList: __;_e\n"
"X-Poedit-Basepath: .\n"
"Plural-Forms: nplurals=2; plural=m>1\n"
"X-Poedit-Language: English\n"
"X-Poedit-Country: UNITED STATES\n"
"X-Poedit-SourceCharset: utf-8\n"
"X-Poedit-SearchPath-0: portfolio-slideshow\n"

#: portfolio-slideshow-admin.php:36
msgid "Thanks for downloading Portfolio Slideshow! If you like it, please be sure to give us a positive rating in the <a href=\"http://wordpress.org/extend/plugins/portfolio-slideshow/\">WordPress repository</a>, it means a lot to us."
msgstr ""

#: portfolio-slideshow-admin.php:37
msgid "If you like Portfolio Slideshow but need more advanced slideshow features, check out our newest plugin, <a href=\"http://madebyraygun.com/lab/portfolio-slideshow\">Portfolio Slideshow Pro</a>."
msgstr ""

#: portfolio-slideshow-admin.php:40
msgid "Portfolio Slideshow"
msgstr ""

#: portfolio-slideshow-admin.php:48
msgid "Slideshow Settings"
msgstr ""

#: portfolio-slideshow-admin.php:49
msgid "Documentation"
msgstr ""

#: portfolio-slideshow-admin.php:54
msgid "Options changed here become the default for all slideshows. Most options can also be changed on a per-slideshow basis by using the slideshow attributes."
msgstr ""

#: portfolio-slideshow-admin.php:56
msgid "Slideshow Display"
msgstr ""

#: portfolio-slideshow-admin.php:59
msgid "Slideshow Size"
msgstr ""

#: portfolio-slideshow-admin.php:59
msgid "The slideshow size refers to the default image sizes that WordPress creates when you upload an image. You can customize these image sizes in the Media Settings control panel."
msgstr ""

#: portfolio-slideshow-admin.php:62
msgid "thumbnail"
msgstr ""

#: portfolio-slideshow-admin.php:63
msgid "medium"
msgstr ""

#: portfolio-slideshow-admin.php:64
msgid "large"
msgstr ""

#: portfolio-slideshow-admin.php:65
msgid "full"
msgstr ""

#: portfolio-slideshow-admin.php:68
msgid "Transition Effects"
msgstr ""

#: portfolio-slideshow-admin.php:71
msgid "Fade"
msgstr ""

#: portfolio-slideshow-admin.php:72
msgid "Scroll Horizontaly"
msgstr ""

#: portfolio-slideshow-admin.php:75
msgid "Transition Speed"
msgstr ""

#: portfolio-slideshow-admin.php:89
msgid "Captions and titles"
msgstr ""

#: portfolio-slideshow-admin.php:91
msgid "Show Titles"
msgstr ""

#: portfolio-slideshow-admin.php:93
msgid "Show Captions"
msgstr ""

#: portfolio-slideshow-admin.php:95
msgid "Show Descriptions"
msgstr ""

#: portfolio-slideshow-admin.php:101
msgid "Slideshow Behavior"
msgstr ""

#: portfolio-slideshow-admin.php:104
msgid "Autoplay timeout"
msgstr ""

#: portfolio-slideshow-admin.php:104
msgid "Anything other than 0 here will turn on autoplay by default. Time is displayed in ms&mdash;e.g. 1000 = 1 second per slide."
msgstr ""

#: portfolio-slideshow-admin.php:107
msgid "Show loading animation"
msgstr ""

#: portfolio-slideshow-admin.php:107
msgid "If you've got a slow connection or lots of images, sometimes the slideshow can take a little while to load. Selecting this option will include a loading gif to show that something is happening. You may want to adjust the padding on the image to center it for your slideshow."
msgstr ""

#: portfolio-slideshow-admin.php:110
msgid "Disable slideshow wrapping"
msgstr ""

#: portfolio-slideshow-admin.php:110
msgid "Play through to the beginning after it gets to the end, or simply stop"
msgstr ""

#: portfolio-slideshow-admin.php:113
msgid "Allow links to external URLs"
msgstr ""

#: portfolio-slideshow-admin.php:113
msgid "Checking this box allows you to add URLs to your images. For example, if you want your slide to link to a portfolio page or to an external site, you would use this feature. This feature disables the <em>click slide to advance</em> function and will cause problems if you've got anything but a URL in the field, so use it wisely."
msgstr ""

#: portfolio-slideshow-admin.php:116
msgid "Update URL with slide numbers"
msgstr ""

#: portfolio-slideshow-admin.php:116
msgid "You can enable this feature to udpate the URL of the page with the slide number. Example: http://example.com/slideshow/#3 will link directly to the third slide in the slideshow."
msgstr ""

#: portfolio-slideshow-admin.php:120
msgid "Navigation"
msgstr ""

#: portfolio-slideshow-admin.php:123
msgid "Navigation Position"
msgstr ""

#: portfolio-slideshow-admin.php:125
msgid "top"
msgstr ""

#: portfolio-slideshow-admin.php:126
msgid "bottom"
msgstr ""

#: portfolio-slideshow-admin.php:127
#: portfolio-slideshow-admin.php:146
msgid "disabled"
msgstr ""

#: portfolio-slideshow-admin.php:131
msgid "Show thumbnails on single posts/pages"
msgstr ""

#: portfolio-slideshow-admin.php:134
msgid "Show thumbnails on homepage/archive pages"
msgstr ""

#: portfolio-slideshow-admin.php:139
msgid "Diagnostics"
msgstr ""

#: portfolio-slideshow-admin.php:142
msgid "jQuery version"
msgstr ""

#: portfolio-slideshow-admin.php:142
msgid "If you're having trouble with the Javascript effects, you can try an older version of jQuery, or disable it altogether. This sometimes helps if you have plugins or themes that rely on their own version of jQuery. Note that the container height calculations for the slideshow rely on features in 1.4.4, so you may experience issues with your container height if you change this from the default."
msgstr ""

#: portfolio-slideshow-admin.php:156
msgid "Save Changes"
msgstr ""

#: portfolio-slideshow-admin.php:165
msgid "General usage"
msgstr ""

#: portfolio-slideshow-admin.php:168
msgid "Portfolio Slideshow Pro demo"
msgstr ""

#: portfolio-slideshow-admin.php:168
msgid "from"
msgstr ""

#: portfolio-slideshow-admin.php:168
msgid "on"
msgstr ""

#: portfolio-slideshow-admin.php:170
msgid "To use the plugin, upload your photos to a post or page using the WordPress media uploader. Use the [portfolio_slideshow] shortcode to display the slideshow in your page or post."
msgstr ""

#: portfolio-slideshow-admin.php:173
msgid "Use the media uploader to add, sort, and delete your photos."
msgstr ""

#: portfolio-slideshow-admin.php:176
msgid "Insert the slideshow by using this shortcode, exactly as shown. Do not insert the photos into the post."
msgstr ""

#: portfolio-slideshow-admin.php:178
msgid "Shortcode Attributes"
msgstr ""

#: portfolio-slideshow-admin.php:179
msgid "If you would like to customize your slideshows on a per-slideshow basis, you can add the following attributes to the shortcode, which will temporarily override the defaults."
msgstr ""

#: portfolio-slideshow-admin.php:181
msgid ""
"\r\n"
"\tTo select a <strong>different page parent ID</strong> to select the images:"
msgstr ""

#: portfolio-slideshow-admin.php:186
msgid "To change the <strong>image size</strong> you would use the size attribute in the shortcode like this:"
msgstr ""

#: portfolio-slideshow-admin.php:190
msgid "Image transition Effects"
msgstr ""

#: portfolio-slideshow-admin.php:194
msgid "(Top secret! You can use this shortcode attribute to supply any transition effect supported by jQuery Cycle, even if they're not in the plugin! List of supported transitions <a href=\"http://jquery.malsup.com/cycle/begin.html\">here</a>."
msgstr ""

#: portfolio-slideshow-admin.php:196
msgid "Show titles, captions, or descriptions:"
msgstr ""

#: portfolio-slideshow-admin.php:199
msgid "(use false to disable)"
msgstr ""

#: portfolio-slideshow-admin.php:201
msgid "Autoplay Timeout"
msgstr ""

#: portfolio-slideshow-admin.php:205
msgid "Disable Slideshow Wrapping"
msgstr ""

#: portfolio-slideshow-admin.php:209
msgid "or enable it like this:"
msgstr ""

#: portfolio-slideshow-admin.php:213
msgid "Show thumbnails:"
msgstr ""

#: portfolio-slideshow-admin.php:217
msgid "or:"
msgstr ""

#: portfolio-slideshow-admin.php:221
msgid "Navigation position:"
msgstr ""

#: portfolio-slideshow-admin.php:225
msgid "alternately, disable navigation with"
msgstr ""

#: portfolio-slideshow-admin.php:229
msgid "Include or exclude slides:"
msgstr ""

#: portfolio-slideshow-admin.php:235
msgid "You need to specify the attachment ID, which you can find in your"
msgstr ""

#: portfolio-slideshow-admin.php:235
#: portfolio-slideshow-admin.php:247
msgid "Media Library"
msgstr ""

#: portfolio-slideshow-admin.php:235
msgid "by hovering over the thumbnail. You can only include attachments which are attached to the current post. Do not use these attributes simultaneously, they are mutually exclusive."
msgstr ""

#: portfolio-slideshow-admin.php:237
msgid "Multiple slideshows per post/page:"
msgstr ""

#: portfolio-slideshow-admin.php:239
msgid "you can insert multiple slideshows per post/page by including different attachment ids in your shortcode."
msgstr ""

#: portfolio-slideshow-admin.php:241
msgid "Example:"
msgstr ""

#: portfolio-slideshow-admin.php:247
msgid "This example will create two slideshows on the page with two sets of images. Remember, the attachment ID can be found in your"
msgstr ""

#: portfolio-slideshow-admin.php:247
msgid "by hovering over the thumbnail. You can only include attachments which are attached to the current post."
msgstr ""

#: portfolio-slideshow-admin.php:254
msgid "You're using Portfolio Slideshow v."
msgstr ""

#: portfolio-slideshow-admin.php:254
msgid "Check out our <a href=\"http://madebyraygun.com/lab/\" target=\"_blank\">other plugins</a>, and if you have any problems, stop by our <a href=\"http://madebyraygun.com/support/forum/\" target=\"_blank\">support forum</a>!"
msgstr ""

#: portfolio-slideshow.php:113
msgid "Slideshow image links to URL:"
msgstr ""

#: portfolio-slideshow.php:177
msgid "Pause"
msgstr ""

#: portfolio-slideshow.php:177
msgid "Play"
msgstr ""

#: portfolio-slideshow.php:179
msgid "Prev"
msgstr ""

#: portfolio-slideshow.php:179
msgid "Next"
msgstr ""

